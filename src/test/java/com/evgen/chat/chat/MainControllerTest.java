package com.evgen.chat.chat;

import com.evgen.chat.chat.controllers.MainController;
import com.evgen.chat.chat.model.AppUser;
import com.evgen.chat.chat.model.Chat;
import com.evgen.chat.chat.model.ChatMessage;
import com.evgen.chat.chat.repository.ChatMessageRepository;
import com.evgen.chat.chat.repository.ChatRepository;
import com.evgen.chat.chat.repository.PartyRepository;
import com.evgen.chat.chat.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.jaas.JaasGrantedAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.servlet.ModelAndView;
import sun.security.acl.PrincipalImpl;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@EnableAutoConfiguration
@EnableJpaRepositories
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {MainControllerTest.class})
public class MainControllerTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private ChatRepository chatRepository;

    @Mock
    private PartyRepository partyRepository;

    @Mock
    private ChatMessageRepository chatMessageRepository;

    @InjectMocks
    private MainController mainController;

    @Test
    public void getChatTest() {
        GrantedAuthority grantedAuthority = new JaasGrantedAuthority("USER", new PrincipalImpl(""));
        Collection<GrantedAuthority> collation = new ArrayList<>();
        collation.add(grantedAuthority);
        User user = new User("name", "Password", collation);
        ModelAndView modelAndView = mainController.getChat(user);

        assertEquals("Не верная страница", modelAndView.getViewName(), "hello");

        assertTrue("Модели должны передаваться", !modelAndView.wasCleared());

    }

    @Test
    public void addUserTest() {
        ModelAndView modelAndView = mainController.addUser(new AppUser());

        assertEquals("Не верная страница", modelAndView.getViewName(), "redirect:/");
    }

    @Test
    public void regTest() {
        ModelAndView modelAndView = mainController.reg();

        assertEquals("Не верная страница", modelAndView.getViewName(), "regestration");
    }

    @Test
    public void chatViewTest() {
        ModelAndView modelAndView = mainController.chatView();

        assertEquals("Не верная страница", modelAndView.getViewName(), "user");
    }

    @Test
    public void getMessageTest() {
        Chat chat = new Chat();
        chat.setChat_id(1);

        AppUser appUser = new AppUser();
        appUser.setName("df");

        ChatMessage chatMessage = new ChatMessage();

        ChatMessage resulMessage = new ChatMessage();

        chatMessage.setMessage("dfsdf");
        chatMessage.setChat(chat);
        chatMessage.setUser(appUser);

        resulMessage = mainController.getMessages(chatMessage);

        assertEquals("Не верная страница", resulMessage.getMessage(), chatMessage.getMessage());

    }

}
