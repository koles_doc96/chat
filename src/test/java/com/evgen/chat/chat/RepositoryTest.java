package com.evgen.chat.chat;


import com.evgen.chat.chat.model.AppUser;
import com.evgen.chat.chat.model.Chat;
import com.evgen.chat.chat.model.ChatMessage;
import com.evgen.chat.chat.model.Party;
import com.evgen.chat.chat.repository.ChatMessageRepository;
import com.evgen.chat.chat.repository.ChatRepository;
import com.evgen.chat.chat.repository.PartyRepository;
import com.evgen.chat.chat.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@EnableAutoConfiguration
@EnableJpaRepositories
@RunWith(SpringJUnit4ClassRunner.class)
public class RepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Before
    public void prepare() {
        partyRepository.deleteAll();
        chatMessageRepository.deleteAll();
        chatRepository.deleteAll();
        userRepository.deleteAll();

        AppUser appUser = new AppUser();
        appUser.setName("name");
        appUser.setPassword("name");
        userRepository.save(appUser);

        Chat chat = new Chat();
        chat.setUser(appUser);
        chat.setName("чат1");
        chatRepository.save(chat);

        Party party = new Party();
        party.setUser(appUser);
        party.setChat(chat);
        partyRepository.save(party);

        ChatMessage message = new ChatMessage();
        message.setChat(chat);
        message.setUser(appUser);
        message.setMessage("message");
        chatMessageRepository.save(message);

        appUser = new AppUser();
        appUser.setName("name2");
        appUser.setPassword("name2");
        userRepository.save(appUser);

        chat = new Chat();
        chat.setUser(appUser);
        chat.setName("чат2");
        chatRepository.save(chat);

        party = new Party();
        party.setUser(appUser);
        party.setChat(chat);
        partyRepository.save(party);

        message = new ChatMessage();
        message.setChat(chat);
        message.setUser(appUser);
        message.setMessage("message");
        chatMessageRepository.save(message);
    }

    @Test
    public void userTest() {

        List<AppUser> appUser = userRepository.findAll();

        assertEquals("Не верное количество пользователей", appUser.size(), 2);

        AppUser user = new AppUser();
        user = userRepository.findByName("name2");

        assertEquals("Пользователя с таким именем не существует", user.getName(), "name2");
    }

    @Test
    public void chatTest() {
        List<Chat> chats = chatRepository.findAll();

        assertEquals("Не верное количество чатов", chats.size(), 2);

        Chat chat = new Chat();

        chat = chatRepository.findByName("чат1");

        assertEquals("Чата с таким именем не существует", chat.getName(), "чат1");
    }

    @Test
    public void partyTest() {

        List<Party> parties = partyRepository.findAll();

        assertEquals("Не верное количество групп", parties.size(), 2);

        Party party = partyRepository.queryUserForParty(userRepository.findByName("name"),chatRepository.findByName("чат1"));

    }

    @Test
    public void messageTest(){

        List<ChatMessage> messages = chatMessageRepository.findAll();

        assertEquals("Не верное количество сообщений", messages.size(), 2);

    }

}
