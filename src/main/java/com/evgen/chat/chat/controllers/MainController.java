package com.evgen.chat.chat.controllers;

import com.evgen.chat.chat.model.AppUser;
import com.evgen.chat.chat.model.Chat;
import com.evgen.chat.chat.model.ChatMessage;
import com.evgen.chat.chat.model.Party;
import com.evgen.chat.chat.repository.ChatMessageRepository;
import com.evgen.chat.chat.repository.ChatRepository;
import com.evgen.chat.chat.repository.PartyRepository;
import com.evgen.chat.chat.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MainController {

    private static final Logger log = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @RequestMapping("/")
    public ModelAndView getChat(@AuthenticationPrincipal User user) {

        List<Chat> chats = chatRepository.findByUser(
                userRepository.findByName(user.getUsername()));

        List<AppUser> users = userRepository.findAll();

        log.debug("Username = {} ", user.getUsername());

        return new ModelAndView("hello")
                .addObject("chats", chats)
                .addObject("users", users);
    }

    @RequestMapping(value = "/add/chat", method = RequestMethod.POST)
    public ModelAndView addChatAndParty(@AuthenticationPrincipal User user, @ModelAttribute Chat chat) {

        log.debug("Chat = {}", chat);

        Party party = new Party();

        AppUser toUser = chat.getUser();

        AppUser loginUser = userRepository.findByName(user.getUsername());

        chat.setUser(loginUser);

        chatRepository.save(chat);

        party.setChat(chat);

        party.setUser(loginUser);

        partyRepository.save(party);

        party = new Party();

        party.setChat(chat);

        party.setUser(toUser);

        partyRepository.save(party);

        return new ModelAndView("redirect:/");

    }

    @RequestMapping(value = "/regestion", method = RequestMethod.POST)
    public ModelAndView addUser(@ModelAttribute AppUser appUser) {

        AppUser user = userRepository.findByName(appUser.getName());
        if(user!=null) {

            return new ModelAndView("redirect:/reg");
        }

        userRepository.save(appUser);

        return new ModelAndView("redirect:/");
    }

    @RequestMapping("/reg")
    public ModelAndView reg() {
        return new ModelAndView("regestration");
    }

    @RequestMapping("/user")
    public ModelAndView chatView() {
        return new ModelAndView("user");
    }

    @RequestMapping(value = {"chat/{name}", "chat/{name}/user"})
    public ModelAndView openChat(@AuthenticationPrincipal User user, @PathVariable String name) {

        Chat chat = chatRepository.findByName(name);

        Party party = partyRepository.queryUserForParty(
                userRepository.findByName(user.getUsername()), chat);

        List<ChatMessage> messages = chatMessageRepository.queryMessUserToUser(
                userRepository.findByName(user.getUsername())
                , party.getUser()
                , chat);

        for (ChatMessage mess : messages) {
            if (mess.getUser().getName().equals(user.getUsername()))
                mess.setSide("right");
            else
                mess.setSide("left");
        }

        ModelAndView modelAndView = new ModelAndView("user")
                .addObject("party", party)
                .addObject("messages", messages);

        return modelAndView;
    }

    @MessageMapping("/message")
    @SendTo("/chat/messages")
    public ChatMessage getMessages(ChatMessage message) {

        Chat chat = chatRepository.findById((int) message.getChat().getChat_id());

        AppUser appUser = userRepository.findByName(message.getUser().getName());

        message.setChat(chat);
        message.setUser(appUser);
        chatMessageRepository.save(message);

        System.out.println(message);
        return message;
    }
}
