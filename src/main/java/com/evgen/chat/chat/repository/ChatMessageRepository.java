package com.evgen.chat.chat.repository;

import com.evgen.chat.chat.model.AppUser;
import com.evgen.chat.chat.model.Chat;
import com.evgen.chat.chat.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Integer> {

    @Query(value = "SELECT m.* FROM chat.message m WHERE (m.user_id=:user_id1 or m.user_id =:user_id2) and m.chat_id=:chat_id", nativeQuery = true)
    List<ChatMessage> queryMessUserToUser(@Param("user_id1") AppUser user, @Param("user_id2") AppUser user2, @Param("chat_id") Chat chat);
}
