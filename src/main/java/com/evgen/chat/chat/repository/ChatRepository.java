package com.evgen.chat.chat.repository;

import com.evgen.chat.chat.model.AppUser;
import com.evgen.chat.chat.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ChatRepository extends JpaRepository<Chat, Integer> {

    @Query(value = "SELECT ch.* FROM chat.party p, chat.chat ch WHERE p.user_id=:user_id AND p.chat_id=ch.chat_id", nativeQuery = true)
    List<Chat> findByUser(@Param("user_id") AppUser user_id);

    Chat findByName(String name);

    Chat findById(int id);
}
