package com.evgen.chat.chat.repository;

import com.evgen.chat.chat.model.AppUser;
import com.evgen.chat.chat.model.Chat;
import com.evgen.chat.chat.model.Party;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PartyRepository extends JpaRepository<Party, Integer> {

    Party findByPartyId(Integer party_id);

    @Query(value = "SELECT * FROM party p WHERE p.user_id <> :user_id and p.chat_id = :chat_id", nativeQuery = true)
    Party queryUserForParty(@Param("user_id") AppUser userId, @Param("chat_id") Chat chat_id);

}
